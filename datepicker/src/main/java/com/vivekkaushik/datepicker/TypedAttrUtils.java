/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vivekkaushik.datepicker;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

import java.util.NoSuchElementException;

public final class TypedAttrUtils {
    /**
     * getIntColor
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return the color value by attrs
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    /**
     * getColor
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return the color value by attrs
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /**
     * getBoolean
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return the bool value by attrs
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /**
     * getInteger
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return the Integer value by attrs
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getString
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return the String value by attrs
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * getDimensionPixelSize
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return the dimesion pixel size by attrs
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return layout dimension by attrs
     */
    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getDimensionValue();
        }
    }

    /**
     * attrNoSuchElement
     *
     * @param attrs attrs
     * @param attrName attrName
     * @return return element by attrs
     */
    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        if (attrs != null) {
            try {
                attr = attrs.getAttr(attrName).get();
            } catch (NoSuchElementException e) {
                e.fillInStackTrace();
            }
        }
        return attr;
    }
}
