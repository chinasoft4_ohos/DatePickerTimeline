package com.vivekkaushik.datepicker;

import com.vivekkaushik.datepicker.adapter.TimelineAdapter;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimelineView extends ListContainer {
    private TimelineAdapter adapter;
    private Context context;
    private Color monthTextColor;
    private Color dateTextColor;
    private Color dayTextColor;
    private Color selectedColor;
    private Color disabledColor;
    private int year;
    private int month;
    private int date;

    /**
     * Constructor
     *
     * @param context context
     */
    public TimelineView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    /**
     * Constructor
     *
     * @param context context
     * @param attrs attrs
     */
    public TimelineView(Context context, AttrSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    /**
     * Constructor
     *
     * @param context context
     * @param attrs attrs
     * @param defStyle defStyle
     */
    public TimelineView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    void init() {
        year = 1970;
        month = 0;
        date = 1;
        DirectionalLayoutManager directionalLayoutManager = new DirectionalLayoutManager();
        directionalLayoutManager.setOrientation(Component.HORIZONTAL);
        setLayoutManager(directionalLayoutManager);
        adapter = new TimelineAdapter(context, this, -1);
        setItemProvider(adapter);
    }

    public Color getMonthTextColor() {
        return monthTextColor;
    }

    public void setMonthTextColor(Color color) {
        this.monthTextColor = color;
    }

    public Color getDateTextColor() {
        return dateTextColor;
    }

    public void setDateTextColor(Color color) {
        this.dateTextColor = color;
    }

    public Color getDayTextColor() {
        return dayTextColor;
    }

    public void setDayTextColor(Color color) {
        this.dayTextColor = color;
    }

    /**
     * setDisabledDateColor
     *
     * @param color color
     */
    public void setDisabledDateColor(Color color) {
        this.disabledColor = color;
    }

    /**
     * getDisabledDateColor
     *
     * @return Color Color
     */
    public Color getDisabledDateColor() {
        return disabledColor;
    }

    public Color getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(Color color) {
        this.selectedColor = color;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDate() {
        return date;
    }

    /**
     * setDateSelectedListener
     *
     * @param listener listener
     */
    public void setOnDateSelectedListener(OnDateSelectedListener listener) {
        adapter.setDateSelectedListener(listener);
    }

    /**
     * set initial date
     *
     * @param year year
     * @param month month
     * @param date date
     */
    public void setInitialDate(int year, int month, int date) {
        this.year = year;
        this.month = month;
        this.date = date;
        invalidate();
    }

    /**
     * Calculates the date position and set the selected background on that date
     *
     * @param activeDate active Date
     */
    public void setActiveDate(Calendar activeDate) {
        try {
            Date initialDate = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(year + "-" + (month + 1) + "-" + this.date);
            long diff = activeDate.getTime().getTime() - initialDate.getTime();
            int position = (int) (diff / (1000 * 60 * 60 * 24));
            adapter.setSelectedPosition(position);
            invalidate();
        } catch (ParseException e) {
            e.fillInStackTrace();
        }
    }

    /**
     * deactivate dates
     *
     * @param deactivatedDates deactivatedDates
     */
    public void deactivateDates(Date[] deactivatedDates) {
        adapter.disableDates(deactivatedDates);
    }
}
