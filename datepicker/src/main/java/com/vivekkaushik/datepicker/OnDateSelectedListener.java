package com.vivekkaushik.datepicker;

public interface OnDateSelectedListener {
    /**
     * on date selected
     *
     * @param year year
     * @param month month
     * @param day day
     * @param dayOfWeek dayOfWeek
     */
    void onDateSelected(int year, int month, int day, int dayOfWeek);

    /**
     * on disabled date selected
     *
     * @param year year
     * @param month month
     * @param day day
     * @param dayOfWeek dayOfWeek
     * @param isDisabled isDisabled
     */
    void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled);
}
