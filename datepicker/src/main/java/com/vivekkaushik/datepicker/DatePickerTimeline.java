package com.vivekkaushik.datepicker;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.Calendar;
import java.util.Date;

public class DatePickerTimeline extends DirectionalLayout {
    private TimelineView timelineView;
    private Context context;

    /**
     * Constructor
     *
     * @param context context
     */
    public DatePickerTimeline(Context context) {
        super(context);
        this.context = context;
        init(null, "");
    }

    /**
     * Constructor
     *
     * @param context context
     * @param attrs attrs
     */
    public DatePickerTimeline(Context context, AttrSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs, "");
    }

    /**
     * Constructor
     *
     * @param context context
     * @param attrs attrs
     * @param defStyleAttr defStyleAttr
     */
    public DatePickerTimeline(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs, defStyleAttr);
    }

    void init(AttrSet attrs, String defStyleAttr) {
        final Component component = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_date_picker_timeline, this, true);
        timelineView = (TimelineView) component.findComponentById(ResourceTable.Id_timelineView);

        // load Default values
        timelineView.setDayTextColor(TypedAttrUtils.getColor(attrs, "dayTextColor", Color.BLACK));
        timelineView.setDateTextColor(TypedAttrUtils.getColor(attrs, "dateTextColor", Color.BLACK));
        timelineView.setMonthTextColor(TypedAttrUtils.getColor(attrs, "monthTextColor", Color.BLACK));
        timelineView.setDisabledDateColor(TypedAttrUtils.getColor(attrs, "disabledColor", Color.GRAY));
        timelineView.deactivateDates(new Date[0]);
        timelineView.invalidate();
    }

    /**
     * Sets the color for date text
     *
     * @param color the color of the date text
     */
    public void setDateTextColor(Color color) {
        timelineView.setDateTextColor(color);
    }

    /**
     * Sets the color for day text
     *
     * @param color the color of the day text
     */
    public void setDayTextColor(Color color) {
        timelineView.setDayTextColor(color);
    }

    /**
     * Sets the color for month
     *
     * @param color the color of the month text
     */
    public void setMonthTextColor(Color color) {
        timelineView.setMonthTextColor(color);
    }

    /**
     * Sets the color for disabled dates
     *
     * @param color the color of the date
     */
    public void setDisabledDateColor(Color color) {
        timelineView.setDisabledDateColor(color);
    }

    /**
     * Register a callback to be invoked when a date is selected.
     *
     * @param listener the callback that will run
     */
    public void setOnDateSelectedListener(OnDateSelectedListener listener) {
        timelineView.setOnDateSelectedListener(listener);
    }

    /**
     * Set a Start date for the calendar (Default, 1 Jan 1970)
     *
     * @param year start year
     * @param month start month
     * @param date start date
     */
    public void setInitialDate(int year, int month, int date) {
        timelineView.setInitialDate(year, month, date);
    }

    /**
     * Set selected background to active date
     *
     * @param date Active Date
     */
    public void setActiveDate(Calendar date) {
        timelineView.setActiveDate(date);
    }

    /**
     * Deactivate dates from the calendar. User won't be able to select
     * the deactivated date.
     *
     * @param dates Array of Dates
     */
    public void deactivateDates(Date[] dates) {
        timelineView.deactivateDates(dates);
    }

    public TimelineView getTimelineView() {
        return timelineView;
    }
}
