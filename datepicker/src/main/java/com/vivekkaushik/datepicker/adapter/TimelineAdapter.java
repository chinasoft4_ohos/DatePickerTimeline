package com.vivekkaushik.datepicker.adapter;

import com.vivekkaushik.datepicker.OnDateSelectedListener;
import com.vivekkaushik.datepicker.ResourceTable;
import com.vivekkaushik.datepicker.TimelineView;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.app.Context;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimelineAdapter extends BaseItemProvider {
    private static final String[] WEEK_DAYS = DateFormatSymbols.getInstance().getShortWeekdays();
    private static final String[] MONTH_NAME = DateFormatSymbols.getInstance().getShortMonths();

    private Context context;
    private Calendar calendar = Calendar.getInstance();
    private TimelineView timelineView;
    private Date[] deactivatedDates;
    private int cornerRadius = 25;

    private OnDateSelectedListener listener;

    private Component selectedView;
    private int selectedPosition;

    /**
     * constructor
     *
     * @param context context
     * @param timelineView timelineView
     * @param selectedPosition selectedPosition
     */
    public TimelineAdapter(Context context, TimelineView timelineView, int selectedPosition) {
        this.context = context;
        this.timelineView = timelineView;
        this.selectedPosition = selectedPosition;
    }

    private void resetCalendar() {
        calendar.set(timelineView.getYear(), timelineView.getMonth(), timelineView.getDate(),
                1, 0, 0);
    }

    /**
     * Set the position of selected date
     *
     * @param selectedPosition active date Position
     */
    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    /**
     * disable dates
     *
     * @param dates dates
     */
    public void disableDates(Date[] dates) {
        if (dates != null) {
            this.deactivatedDates = dates.clone();
            notifyDataChanged();
        }

    }

    /**
     * setDateSelectedListener
     *
     * @param listener listener
     */
    public void setDateSelectedListener(OnDateSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int position, Component componentP, ComponentContainer componentContainer) {
        ViewHolder viewHolder = null;
        Component component = componentP;
        if (component == null) {
            component = LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_timeline_item_layout, null, false);
            viewHolder = new ViewHolder(component);
            component.setTag(viewHolder);
        } else {
            if (component.getTag() instanceof ViewHolder) {
                viewHolder = (ViewHolder) component.getTag();
            }
        }
        if (viewHolder != null) {
            resetCalendar();
            calendar.add(Calendar.DAY_OF_YEAR, position);
            final int year = calendar.get(Calendar.YEAR);
            final int month = calendar.get(Calendar.MONTH);
            final int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            final int day = calendar.get(Calendar.DAY_OF_MONTH);

            final boolean isDisabled = viewHolder.bind(month, day, dayOfWeek, year, position);
            viewHolder.rootView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (selectedView != null) {
                        selectedView.setBackground(null);
                    }
                    if (!isDisabled) {
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setShape(ShapeElement.RECTANGLE);
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(0x2E000000));
                        shapeElement.setCornerRadius(cornerRadius);
                        component.setBackground(shapeElement);
                        selectedPosition = position;
                        selectedView = component;

                        if (listener != null) {
                            listener.onDateSelected(year, month, day, dayOfWeek);
                        }
                    } else {
                        if (listener != null) {
                            listener.onDisabledDateSelected(year, month, day, dayOfWeek, isDisabled);
                        }
                    }
                }
            });
        }
        return component;
    }

    class ViewHolder {
        private final Text monthView;
        private final Text dateView;
        private final Text dayView;
        private Component rootView;

        ViewHolder(Component component) {
            monthView = (Text) component.findComponentById(ResourceTable.Id_monthView);
            dateView = (Text) component.findComponentById(ResourceTable.Id_dateView);
            dayView = (Text) component.findComponentById(ResourceTable.Id_dayView);
            rootView = component.findComponentById(ResourceTable.Id_rootView);
        }

        boolean bind(int month, int day, int dayOfWeek, int year, int position) {
            monthView.setTextColor(timelineView.getMonthTextColor());
            dateView.setTextColor(timelineView.getDateTextColor());
            dayView.setTextColor(timelineView.getDayTextColor());

            dayView.setText(WEEK_DAYS[dayOfWeek].toUpperCase(Locale.CHINA));
            monthView.setText(MONTH_NAME[month].toUpperCase(Locale.CHINA));
            dateView.setText(String.valueOf(day));
            dateView.setFont(Font.DEFAULT_BOLD);

            if (selectedPosition == position) {
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setShape(ShapeElement.RECTANGLE);
                shapeElement.setRgbColor(RgbColor.fromArgbInt(0x2E000000));
                shapeElement.setCornerRadius(cornerRadius);
                rootView.setBackground(shapeElement);
                selectedView = rootView;
            } else {
                rootView.setBackground(null);
            }

            for (Date date : deactivatedDates) {
                Calendar tempCalendar = Calendar.getInstance();
                tempCalendar.setTime(date);
                if (tempCalendar.get(Calendar.DAY_OF_MONTH) == day
                        && tempCalendar.get(Calendar.MONTH) == month
                        && tempCalendar.get(Calendar.YEAR) == year) {
                    monthView.setTextColor(timelineView.getDisabledDateColor());
                    dateView.setTextColor(timelineView.getDisabledDateColor());
                    dayView.setTextColor(timelineView.getDisabledDateColor());
                    rootView.setBackground(null);
                    return true;
                }
            }
            return false;
        }
    }
}