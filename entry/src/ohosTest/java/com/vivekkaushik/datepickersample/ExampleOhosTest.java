package com.vivekkaushik.datepickersample;

import com.vivekkaushik.datepicker.DatePickerTimeline;
import ohos.agp.utils.Color;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    /**
     * testsetDateTextColor
     */
    @Test
    public void testsetDateTextColor() {
        DatePickerTimeline datePickerTimeline = new DatePickerTimeline(MyApplication.getInstance());
        datePickerTimeline.setDateTextColor(Color.RED);
        assertEquals(Color.RED, datePickerTimeline.getTimelineView().getDateTextColor());
    }

    /**
     * testsetDisabledDateColor
     */
    @Test
    public void testsetDisabledDateColor() {
        DatePickerTimeline datePickerTimeline = new DatePickerTimeline(MyApplication.getInstance());
        datePickerTimeline.setDisabledDateColor(Color.RED);
        assertEquals(Color.RED, datePickerTimeline.getTimelineView().getDisabledDateColor());
    }

    /**
     * testsetDayTextColor
     */
    @Test
    public void testsetDayTextColor() {
        DatePickerTimeline datePickerTimeline = new DatePickerTimeline(MyApplication.getInstance());
        datePickerTimeline.setDayTextColor(Color.RED);
        assertEquals(Color.RED, datePickerTimeline.getTimelineView().getDayTextColor());
    }

    /**
     * testsetMonthTextColor
     */
    @Test
    public void testsetMonthTextColor() {
        DatePickerTimeline datePickerTimeline = new DatePickerTimeline(MyApplication.getInstance());
        datePickerTimeline.setMonthTextColor(Color.RED);
        assertEquals(Color.RED, datePickerTimeline.getTimelineView().getMonthTextColor());
    }

    /**
     * testsetInitialDate
     */
    @Test
    public void testsetInitialDate() {
        DatePickerTimeline datePickerTimeline = new DatePickerTimeline(MyApplication.getInstance());
        datePickerTimeline.setInitialDate(2021, 04, 26);
        assertEquals(2021, datePickerTimeline.getTimelineView().getYear());
        assertEquals(04, datePickerTimeline.getTimelineView().getMonth());
        assertEquals(26, datePickerTimeline.getTimelineView().getDate());
    }
}