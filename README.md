# DatePickerTimeline

#### 项目介绍
- 项目名称：DatePickerTimeline
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个可水平滑动的日期选择控件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v0.0.4

#### 效果演示
<img src="datepickertimeline.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:DatePickerTimeline:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在布局中引入自定义组件，例如：
```示例XML
    <com.vivekkaushik.datepicker.DatePickerTimeline
        ohos:id="$+id:dateTimeline"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:end_margin="8vp"
        ohos:start_margin="8vp"
        ohos:top_margin="8vp"
        ohos:vertical_center="true"/>
```
JAVA
```java
        DatePickerTimeline datePickerTimeline = (DatePickerTimeline) findComponentById(ResourceTable.Id_dateTimeline);
        datePickerTimeline.setInitialDate(2021, 04, 26);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, 5);
        datePickerTimeline.setActiveDate(date);
        datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                HiLog.info(logLabel, "onDateSelected: " + day);
            }

            @Override
            public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                HiLog.info(logLabel, "onDisabledDateSelected: : " + day);
            }
        });
        Date[] dates = {Calendar.getInstance().getTime()};
        datePickerTimeline.deactivateDates(dates);
```
You can use the following properties in your XML to change your DatePickerTimeline colors.
```示例XML
app:dateTextColor (color) -> default -> Black
app:dayTextColor (color) -> default -> Black
app:monthTextColor (color) -> default -> Black
app:disabledColor (color) -> default -> Grey
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0

## 版权和许可信息
- Apache 2.0



